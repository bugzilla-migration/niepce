/** @brief what to generate bindings for in Rust */

#include "properties-enum.hpp"

#include "engine/db/libmetadata.hpp"
#include "engine/library/notification.hpp"
#include "fwk/utils/files.hpp"
