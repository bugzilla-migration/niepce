/*
 * niepce - fwk/utils/exempi.rs
 *
 * Copyright (C) 2017 Hubert Figuière
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use std::fs::File;
use std::io::prelude::*;
use std::path::Path;

use chrono::{DateTime, Utc};
use exempi;
use exempi::Xmp;

static NIEPCE_XMP_NAMESPACE: &'static str = "http://xmlns.figuiere.net/ns/niepce/1.0";
static NIEPCE_XMP_NS_PREFIX: &'static str = "niepce";
static UFRAW_INTEROP_NAMESPACE: &'static str = "http://xmlns.figuiere.net/ns/ufraw_interop/1.0";
static UFRAW_INTEROP_NS_PREFIX: &'static str = "ufrint";


pub static NS_TIFF: &'static str = "http://ns.adobe.com/tiff/1.0/";
pub static NS_XAP: &'static str = "http://ns.adobe.com/xap/1.0/";
pub static NS_EXIF: &'static str = "http://ns.adobe.com/exif/1.0/";
pub static NS_DC: &'static str = "http://purl.org/dc/elements/1.1/";
pub static NS_AUX: &'static str = "http://ns.adobe.com/exif/1.0/aux/";

pub struct NsDef {
    ns: String,
    prefix: String,
}

pub struct ExempiManager {
}

impl ExempiManager {
    pub fn new(namespaces: Option<Vec<NsDef>>) -> ExempiManager {
        if exempi::init() {
            exempi::register_namespace(NIEPCE_XMP_NAMESPACE, NIEPCE_XMP_NS_PREFIX);
            exempi::register_namespace(UFRAW_INTEROP_NAMESPACE, UFRAW_INTEROP_NS_PREFIX);
        }
        if let Some(nslist) = namespaces {
            for nsdef in nslist {
                // TOOD check the return value
                exempi::register_namespace(nsdef.ns.as_str(), nsdef.prefix.as_str());
            }
        }
        ExempiManager {}
    }
}

impl Drop for ExempiManager {
    fn drop(&mut self) {
        exempi::terminate();
    }
}


pub struct XmpMeta {
    pub xmp: Xmp,
    keywords: Vec<String>,
    keywords_fetched: bool,
}


impl XmpMeta {
    pub fn new() -> XmpMeta {
        XmpMeta {
            xmp: exempi::Xmp::new(),
            keywords: Vec::new(),
            keywords_fetched: false,
        }
    }

    pub fn new_from_file(file: &str, sidecar_only: bool) -> Option<XmpMeta> {
        let mut meta: Option<XmpMeta> = None;
        if !sidecar_only {
            if let Some(xmpfile) = exempi::XmpFile::open_new(file, exempi::OPEN_READ) {
                if let Some(xmp) = xmpfile.get_new_xmp() {
                    meta = Some(XmpMeta {
                        xmp: xmp,
                        keywords: Vec::new(),
                        keywords_fetched: false
                    });
                }
            }
        }

        let mut sidecar_meta: Option<XmpMeta> = None;
        let filepath = Path::new(file);
        let sidecar = filepath.with_extension("xmp");
        let sidecaropen = File::open(sidecar);
        if let Some(mut sidecarfile) = sidecaropen.ok() {
            let mut sidecarcontent = String::new();
            let result = sidecarfile.read_to_string(&mut sidecarcontent);
            if result.ok().is_some() {
                let mut xmp = exempi::Xmp::new();
                if xmp.parse(sidecarcontent.into_bytes().as_slice()) {
                    sidecar_meta = Some(XmpMeta {
                        xmp: xmp,
                        keywords: Vec::new(),
                        keywords_fetched: false
                    });
                }
            }
        }
        if meta.is_none() || sidecar_meta.is_none() {
            if meta.is_some() {
                return meta;
            }
            return sidecar_meta;

        } else {
            let mut final_meta = sidecar_meta.unwrap();
            if !meta.as_ref().unwrap().merge_missing_into_xmp(&mut final_meta) {
                err_out!("xmp merge failed");
            } else {
                return Some(final_meta);
            }
        }
        // XXX maybe we should be more permissible. Returning the
        // built-in meta if merge failed might not be the best option.
        None
    }

    ///
    /// Merge missing properties from self (source) to destination
    /// struct and array are considerd missing as a whole, not their content.
    ///
    pub fn merge_missing_into_xmp(&self, dest: &mut XmpMeta) -> bool {
        // Merge XMP self into the dest that has more recent.
        let source_date = self.get_date_property(NS_XAP, "MetadataDate");
        let dest_date = dest.get_date_property(NS_XAP, "MetadataDate");

        if source_date.is_none() || dest_date.is_none() {
            dbg_out!("missing metadata date {} {}", source_date.is_some(),
                     dest_date.is_some());
            return false;
        }
        if source_date > dest_date {
            dbg_out!("file meta is more recent than sidecar");
            return false;
        }

        // in properties in source not in destination gets copied over.
        let mut iter = exempi::XmpIterator::new(&self.xmp, "", "", exempi::ITER_PROPERTIES);
        {
            use exempi::XmpString;
            let mut schema = XmpString::new();
            let mut name = XmpString::new();
            let mut value = XmpString::new();
            let mut option = exempi::PROP_NONE;
            while iter.next(&mut schema, &mut name, &mut value, &mut option) {
                if name.to_str().is_empty() {
                    continue;
                }
                if option.contains(exempi::PROP_VALUE_IS_ARRAY)
                      || option.contains(exempi::PROP_VALUE_IS_STRUCT) {
                    iter.skip(exempi::ITER_SKIP_SUBTREE);
                    continue;
                }

                if !dest.xmp.has_property(schema.to_str(), name.to_str()) {
                    dest.xmp.set_property(schema.to_str(), name.to_str(),
                                          value.to_str(), exempi::PROP_NONE);
                }
            }
        }

        true
    }

    pub fn serialize_inline(&self) -> String {
        if let Some(xmpstr) = self.xmp.serialize_and_format(
            exempi::SERIAL_OMITPACKETWRAPPER | exempi::SERIAL_OMITALLFORMATTING,
            0, "", "", 0) {
            let buf = String::from(xmpstr.to_str());
            return buf;
        }
        String::new()
    }

    pub fn serialize(&self) -> String {
        if let Some(xmpstr) = self.xmp.serialize_and_format(
            exempi::SERIAL_OMITPACKETWRAPPER, 0, "\n", "", 0) {
            let buf = String::from(xmpstr.to_str());
            return buf;
        }
        String::new()
    }

    pub fn unserialize(&mut self, buf: &str) -> bool {
        self.xmp.parse(buf.as_bytes())
    }

    pub fn orientation(&self) -> Option<i32> {
        let mut flags: exempi::PropFlags = exempi::PropFlags::empty();
        return self.xmp.get_property_i32(NS_TIFF, "Orientation", &mut flags);
    }

    pub fn label(&self) -> Option<String> {
        let mut flags: exempi::PropFlags = exempi::PROP_NONE;
        let xmpstring = try_opt!(self.xmp.get_property(NS_XAP, "Label", &mut flags));
        Some(String::from(xmpstring.to_str()))
    }

    pub fn rating(&self) -> Option<i32> {
        let mut flags: exempi::PropFlags = exempi::PROP_NONE;
        return self.xmp.get_property_i32(NS_XAP, "Rating", &mut flags);
    }

    pub fn flag(&self) -> Option<i32> {
        let mut flags: exempi::PropFlags = exempi::PropFlags::empty();
        return self.xmp.get_property_i32(NIEPCE_XMP_NAMESPACE, "Flag", &mut flags);
    }

    pub fn creation_date(&self) -> Option<DateTime<Utc>> {
        let mut flags: exempi::PropFlags = exempi::PropFlags::empty();
        let xmpstring = try_opt!(self.xmp.get_property(NS_EXIF, "DateTimeOriginal", &mut flags));
        let date = try_opt!(DateTime::parse_from_rfc3339(xmpstring.to_str()).ok());

        return Some(date.with_timezone(&Utc));
    }

    pub fn creation_date_str(&self) -> Option<String> {
        let mut flags: exempi::PropFlags = exempi::PropFlags::empty();
        let xmpstring = try_opt!(self.xmp.get_property(NS_EXIF, "DateTimeOriginal", &mut flags));
        Some(String::from(xmpstring.to_str()))
    }

    /// Get the date property and return a DateTime<Utc> parsed
    /// from the string value.
    pub fn get_date_property(&self, ns: &str, property: &str) -> Option<DateTime<Utc>> {
        let mut flags: exempi::PropFlags = exempi::PropFlags::empty();
        let xmpstring = try_opt!(self.xmp.get_property(ns, property, &mut flags));
        let date = try_opt!(DateTime::parse_from_rfc3339(xmpstring.to_str()).ok());
        return Some(date.with_timezone(&Utc));
    }

    pub fn keywords(&mut self) -> &Vec<String> {
        if !self.keywords_fetched {
            use exempi::XmpString;

            let mut iter = exempi::XmpIterator::new(&mut self.xmp, NS_DC, "subject",
                                                    exempi::ITER_JUST_LEAF_NODES);
            let mut schema = XmpString::new();
            let mut name = XmpString::new();
            let mut value = XmpString::new();
            let mut option = exempi::PROP_NONE;
            while iter.next(&mut schema, &mut name, &mut value, &mut option) {
                self.keywords.push(String::from(value.to_str()));
            }
            self.keywords_fetched = true;
        }
        &self.keywords
    }
}

pub fn gps_coord_from_xmp(xmps: &str) -> Option<f64> {
    let mut current: &str = xmps;
    let degs: &str;

    // step 1 - degrees
    let sep = try_opt!(current.find(','));
    let (d, remainder) = current.split_at(sep);
    current = remainder;
    degs = d;

    // step 2 - minutes
    if current.len() < 1 {
        return None;
    }
    // get rid of the comma
    let (_, current) = current.split_at(1);
    let orientation = match current.chars().last() {
        Some('N') | Some('E') =>
            1.0f64,
        Some('S') | Some('W') =>
            -1.0f64,
        _ => return None,
    };

    // extract minutes. There are two formats
    let fminutes: f64;
    if let Some(sep) = current.find(',') {
        // DD,mm,ss format
        if sep >= (current.len() - 1) {
            return None;
        }
        if current.len() <= 2 {
            return None;
        }
        let (minutes, seconds) = current.split_at(sep);
        let (_, seconds) = seconds.split_at(1);
        let (seconds, _) = seconds.split_at(seconds.len() - 1);
        let m = try_opt!(minutes.parse::<f64>().ok());
        let s = try_opt!(seconds.parse::<f64>().ok());
        fminutes = m + (s / 60f64);
    } else {
        // DD,mm.mm format
        let (minutes, _) = current.split_at(current.len() - 1);
        let m = try_opt!(minutes.parse::<f64>().ok());
        fminutes = m;
    }

    let mut deg = try_opt!(degs.parse::<f64>().ok());
    if deg > 180.0 {
        return None;
    }
    deg += fminutes / 60.0;
    deg *= orientation;

    return Some(deg);
}

#[no_mangle]
pub extern "C" fn fwk_exempi_manager_new() -> *mut ExempiManager {
    return Box::into_raw(Box::new(ExempiManager::new(None)))
}

#[no_mangle]
pub extern "C" fn fwk_exempi_manager_delete(em: *mut ExempiManager) {
    unsafe { Box::from_raw(em); }
}

#[cfg(test)]
mod tests {
    use std::path::PathBuf;
    use super::ExempiManager;
    use super::XmpMeta;
    use exempi;

    fn get_xmp_sample_path() -> PathBuf {
        use std::env;

        let mut dir: PathBuf;
        if let Ok(pdir) = env::var("CARGO_MANIFEST_DIR") {
            dir = PathBuf::from(pdir);
            dir.push("src");
            dir.push("fwk");
            dir.push("utils");
        } else {
            dir = PathBuf::from(".");
        }
        dir
    }

    #[test]
    fn xmp_meta_works() {

        let mut dir = get_xmp_sample_path();
        dir.push("test.xmp");
        let _xmp_manager = ExempiManager::new(None);

        if let Some(xmpfile) = dir.to_str() {
            let meta = XmpMeta::new_from_file(xmpfile, true);

            assert!(meta.is_some());
            let mut meta = meta.unwrap();
            assert_eq!(meta.orientation().unwrap_or(0), 1);
            // test keywords()
            let keywords = meta.keywords();
            assert_eq!(keywords.len(), 5);
            assert_eq!(keywords[0], "choir");
            assert_eq!(keywords[1], "night");
            assert_eq!(keywords[2], "ontario");
            assert_eq!(keywords[3], "ottawa");
            assert_eq!(keywords[4], "parliament of canada");
        } else {
            assert!(false);
        }
    }

    fn test_property_value(meta: &XmpMeta, ns: &str,
                           property: &str, expected_value: &str) {
        let mut flags: exempi::PropFlags = exempi::PropFlags::empty();
        let value = meta.xmp.get_property(ns, property, &mut flags);
        assert!(value.is_some());
        assert_eq!(value.unwrap().to_str(), expected_value);
    }

    fn test_property_array_value(meta: &XmpMeta, ns: &str,
                                 property: &str, idx: i32, expected_value: &str) {
        let mut flags: exempi::PropFlags = exempi::PropFlags::empty();
        let value = meta.xmp.get_array_item(ns, property, idx, &mut flags);
        assert!(value.is_some());
        assert_eq!(value.unwrap().to_str(), expected_value);
    }

    #[test]
    fn test_merge_missing_into_xmp() {
        let dir = get_xmp_sample_path();

        let mut source = dir.clone();
        source.push("test.xmp");

        let mut dest = dir.clone();
        dest.push("test2.xmp");
        let _xmp_manager = ExempiManager::new(None);

        if let Some(xmpfile) = source.to_str() {
            let meta = XmpMeta::new_from_file(xmpfile, true);
            assert!(meta.is_some());
            let meta = meta.unwrap();

            if let Some(xmpfile) = dest.to_str() {
                let dstmeta = XmpMeta::new_from_file(xmpfile, true);
                assert!(dstmeta.is_some());
                let mut dstmeta = dstmeta.unwrap();

                let result = meta.merge_missing_into_xmp(&mut dstmeta);
                assert!(result);
                // properties that were missing
                test_property_value(&dstmeta, super::NS_TIFF, "Model", "Canon EOS 20D");
                test_property_value(&dstmeta, super::NS_AUX, "Lens", "24.0-85.0 mm");

                // Array property that contain less in destination
                // Shouldn't have changed.
                test_property_array_value(&dstmeta, super::NS_DC, "subject", 1, "night");
                test_property_array_value(&dstmeta, super::NS_DC, "subject", 2, "ontario");
                test_property_array_value(&dstmeta, super::NS_DC, "subject", 3, "ottawa");
                test_property_array_value(&dstmeta, super::NS_DC, "subject", 4, "parliament of canada");
                assert!(!dstmeta.xmp.has_property(super::NS_DC, "dc:subject[5]"));
            }
        } else {
            assert!(false);
        }
    }

    #[test]
    fn gps_coord_from_works() {
        use super::gps_coord_from_xmp;

        let mut output = gps_coord_from_xmp("foobar");
        assert!(output.is_none());

        // malformed 1
        output = gps_coord_from_xmp("45,29.6681666667");
        assert!(output.is_none());

        // malformed 2
        output = gps_coord_from_xmp("45,W");
        assert!(output.is_none());

        // malformed 3
        output = gps_coord_from_xmp("45,29,N");
        assert!(output.is_none());

        // out of bounds
        output = gps_coord_from_xmp("200,29.6681666667N");
        assert!(output.is_none());

        // well-formed 1
        output = gps_coord_from_xmp("45,29.6681666667N");
        assert!(output.is_some());
        assert_eq!(output.unwrap(), 45.494469444445002181964810006320476531982421875);

        // well-formed 2
        output = gps_coord_from_xmp("73,38.2871666667W");
        assert!(output.is_some());
        assert_eq!(output.unwrap(), -73.6381194444449960201382054947316646575927734375);

        // well-formed 3
        output = gps_coord_from_xmp("45,29,30.45N");
        assert!(output.is_some());
        assert_eq!(output.unwrap(), 45.49179166666666418450404307805001735687255859375);
    }
}
